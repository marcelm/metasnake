#!/usr/bin/env python3
"""
Make it possible to write Snakemake rules in pure Python, using metaprogramming
magic.
"""
import sys
from snakemake.workflow import Workflow, Rules
from snakemake.exceptions import RuleException
import snakemake.workflow
from snakemake import shell
from snakemake.logging import setup_logger

setup_logger()

_workflow = Workflow(__file__)
snakemake.workflow.rules = Rules()
snakemake.workflow.config = dict()

_workflow.debug = True


class FuncRunner:
	"""This wraps the SnakeRule.run function so that it can be pickled"""
	def __init__(self, clsobj):
		self.clsobj = clsobj

	def __call__(self, *args, **kwargs):
		return self.clsobj.run(*args, **kwargs)

	__code__ = __call__.__code__


class ShellCommandRunner:
	def __init__(self, shellcmd):
		self._shellcmd = shellcmd

	def __call__(self, input, output, params, wildcards, threads, resources, log, version):
		shell(self._shellcmd)

	__code__ = __call__.__code__


class MetaRule(type):
	"""
	This metaclass ensures that a snakemake rule is created and registered when
	a class that uses it is defined. See also the docstring for Rule below.
	"""
	def __prepare__(cls, name):
		"""
		Inject setter functions such as input(), output() etc. into
		the class dictionary. When invoked, the setters set corresponding
		attributes _input, _output etc.
		"""
		classdict = dict()
		def make_setter_func(attr_name):
			def setter(*args, **kwargs):
				classdict['_' + attr_name] = (args, kwargs)
			return setter
		for attr_name in ('input', 'output', 'params', 'resources', 'log'):
			classdict[attr_name] = make_setter_func(attr_name)
		return classdict

	def __new__(cls, name, bases, attrs):
		"""
		Return a new class object, but also inspect the attributes that were
		set during class definition and create a snakemake rule from them.
		"""
		# Create the class first
		clsobj = super().__new__(cls, name, bases, attrs)
		if not bases:
			# Do not register the base class as a rule
			return clsobj

		# The following code is a simplified version of what snakemake does in
		# Workflow.rule.
		# TODO The error-checking code is duplicated here, would be nice if
		# that were not necessary.
		_workflow.add_rule(name=name, lineno=0, snakefile='TODO')
		rule = _workflow.get_rule(name)
		rule.docstring = clsobj.__doc__
		rule.priority = clsobj.priority
		rule.version = clsobj.version
		rule.message = clsobj.message
		rule.set_input(*clsobj._input[0], **clsobj._input[1])
		rule.set_output(*clsobj._output[0], **clsobj._output[1])
		rule.set_params(*clsobj._params[0], **clsobj._params[1])
		rule.set_log(*clsobj._log[0], **clsobj._log[1])
		if not isinstance(clsobj.threads, int):
			raise TypeError("Threads value has to be an integer.")
		rule.resources["_cores"] = clsobj.threads
		if clsobj._resources[0]:
			raise TypeError("Resources must be named.")
		resources = clsobj._resources[1]
		if not all(map(lambda r: isinstance(r, int), resources.values())):
			raise TypeError("Resources values have to be integers.")
		rule.resources.update(resources)

		rule.docstring = clsobj.__doc__
		if clsobj.shellcmd:
			rule.shellcmd = clsobj.shellcmd
			rule.run_func = ShellCommandRunner(clsobj.shellcmd)
		else:
			rule.run_func = FuncRunner(clsobj)
		#rule.run_func.__name__ = ...  # TODO what is this used for?

		if not isinstance(clsobj.priority, (int, float)):
			raise TypeError("Priority values have to be numeric.")
		if hasattr(clsobj, 'benchmark'):
			# Simply setting it to None does not work since assignment has side-effects
			rule.benchmark = clsobj.benchmark
		if not isinstance(rule.norun, bool):
			raise TypeError('norun must be a bool')

		#ruleinfo.func.__name__ = "__{}".format(name)  # TODO is this needed?
		_workflow.globals[name] = rule.run_func
		setattr(snakemake.workflow.rules, name, rule.run_func)
		return clsobj


class Rule(metaclass=MetaRule):
	"""
	Base class for user-defined rules. The parameters that we set here are
	defaults that can then be overwritten in derived classes.

	To use a Rule, declare a class that derives from it.

	At **declaration time**, a snakemake.workflow.Rule instance is created and
	registered. As in normal Snakefiles, the first Rule subclass that is defined
	creates the top-level rule that is executed if no target is given.
	"""
	input()
	output()
	resources()
	log()
	params()

	priority = 1
	threads = 1
	norun = False
	message = None
	shellcmd = None
	version = None
	# do not set benchmark = None here

	def run(input, output, params, wildcards, threads, resources, log, version):
		pass


class WorkflowProxy:
	"""
	A wrapper around the Workflow object that calls .check() automatically and
	fills in some default parameters when running .execute.
	"""
	def __init__(self, workflow):
		self._workflow = workflow
		self._checked = False

	def execute(self, dryrun=False):
		if not self._checked:
			self._workflow.check()
			self._checked = True
		self._workflow.execute(
			dryrun=dryrun,
			updated_files=[],
			resources=dict(),
			benchmark_repeats=1)


workflow = WorkflowProxy(_workflow)
