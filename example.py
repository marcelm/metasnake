#!/usr/bin/env python3
from metasnake import workflow, shell, Rule

shell.prefix("set -euo pipefail;")


class All(Rule):
	input("reads.counts")


class UnpackFastq(Rule):
	"""Unpack a FASTQ file"""

	input("{file}.fastq.gz")
	output("{file}.fastq")

	resources(time=60, memory=100)
	params(p="{file}.params")
	threads = 8
	log('unpack.log')
	benchmark = '{file}.json'

	shellcmd = \
		"""
		zcat {input} > {output}
		echo finished > {log}
		"""


class CountReads(Rule):
	"""Count reads in a FASTQ file"""

	input(fastq="{file}.fastq")
	output(counts="{file}.counts")

	message = "Counting the reads ..."
	threads = 4
	resources(time=60, memory=120)

	def run(input, output, params, wildcards, threads, resources, log, version):
		n = 0
		with open(input.fastq) as f:
			for _ in f:
				n += 1
		with open(output.counts, 'w') as f:
			print(n / 4, file=f)


if __name__ == '__main__':
	print("Dry run ...")
	workflow.execute(dryrun=True)
	print("Running workflow ...")
	workflow.execute(dryrun=False)
