metasnake: Snakemake rules in pure Python
=========================================

This module makes it possible to write
[Snakemake](https://bitbucket.org/johanneskoester/snakemake/) rules in pure
Python, using a declarative syntax very similar to the one in regular
Snakefiles.

Instead of using the `rule:` syntax to define a rule, you declare a subclass
of `Rule`:

	from metasnake import Rule, workflow

	class DoSomething(Rule):
		input('source.txt')
		output('destination.txt')
		shellcmd = "cp {input} {output}"

	workflow.execute()

This corresponds to the following Snakefile:

	rule DoSomething:
		input: 'source.txt'
		output: 'destination.txt'
		shell: "cp {input} {output}"

This is a proof of concept to demonstrate how this could look like. It works on
those few examples I tested it on, but it will probably fail in various weird
ways if you use it. There are also many limitations. For example, except for the
dry-run option, none of the snakemake parameters that are set via command-line
arguments are accessible, making it impossible to set the number of threads etc.

The implementation uses Python’s ability for metaprogramming (hence the name of
this module). With it, it is possible to influence how classes are created. In
this case, we use it to create a snakemake rule everytime a subclass of Rule is
declared.

As in regular Snakefiles, the first rule that is defined is considered to be
the top-level rule.

See `example.py` for a more complete example. Run it with:

	python3 example.py


Contact: marcel.martin@scilifelab.se
